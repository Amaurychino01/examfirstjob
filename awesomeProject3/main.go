package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"log"
	"net/http"
)

type Persona struct {
	idpersona int    `json:"idpersona"`
	nombre    string `json:"nombre"`
	paterno   string `json:"paterno"`
	materno   string `json:"materno"`
	edad      int    `json:"edad"`
}
type JsonResponse struct {
	Type    string    `json:"type"`
	Data    []Persona `json:"data"`
	Message string    `json:"message"`
}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "12345"
	dbname   = "postgres"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func OpenConnection() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)

	checkErr(err)

	return db
}

func Get(w http.ResponseWriter, r *http.Request) {
	db := OpenConnection()

	rows, err := db.Query("SELECT DISTINCT * FROM persona")

	// check errors
	checkErr(err)

	// var response []JsonResponse
	var people []Persona

	// Foreach movie
	for rows.Next() {
		var Idpersona int
		var Nombre string
		var Paterno string
		var Materno string
		var Edad int
		err = rows.Scan(&Idpersona, &Nombre, &Paterno, &Materno, &Edad)
		people = append(people, Persona{idpersona: Idpersona, nombre: Nombre,
			paterno: Paterno, materno: Materno, edad: Edad})
	}
	var response = JsonResponse{Type: "success", Data: people}

	json.NewEncoder(w).Encode(response)
}

func insert(w http.ResponseWriter, r *http.Request) {

	nombre := r.FormValue("nombre")
	paterno := r.FormValue("paterno")
	materno := r.FormValue("materno")
	edad := r.FormValue("edad")

	var response = JsonResponse{}

	if nombre == "" {
		response = JsonResponse{Type: "error", Message: "error."}
	} else {
		db := OpenConnection()

		var lastInsertID int
		err := db.QueryRow("INSERT INTO movies(nombre, paterno,materno, edad, ) VALUES($1,$2,$3,$4)m", nombre, paterno, materno, edad).Scan(&lastInsertID)

		// check errors
		checkErr(err)

		response = JsonResponse{Type: "success", Message: "The movie has been inserted successfully!"}
	}

	json.NewEncoder(w).Encode(response)
}
func delete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	idpersona := params["idpersona"]

	var response = JsonResponse{}

	if idpersona == "" {
		response = JsonResponse{Type: "error", Message: "error"}
	} else {
		db := OpenConnection()

		_, err := db.Exec("DELETE FROM persona where idpersona = ?", idpersona)

		// check errors
		checkErr(err)

		response = JsonResponse{Type: "success", Message: "The movie has been deleted successfully!"}
	}

	json.NewEncoder(w).Encode(response)
}

func main() {
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", Get).Methods("GET")
	router.HandleFunc("/insert", insert).Methods("POST")
	router.HandleFunc("/delete", insert).Methods("POST")

	log.Fatal(http.ListenAndServe(":3000", router))
}
