import psycopg2
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)

CORS(app)
try:
    conection = psycopg2.connect(
        host='localhost',
        user='postgres',
        password='12345',
        database='postgres'
    )
    print("conexion exitosa")
except Exception as ex:
    print(ex)

cursor = conection.cursor()

@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'

@app.route('/api/select')
def select():
    sql = 'select * from persona'
    cursor.execute(sql)
    registro = cursor.fetchall()
    cursor.close()
    return jsonify(registro)


@app.route('/api/create', methods = ['POST'])
def insert():
    persona = request.get_json()
    nombre=persona['nombre']
    paterno=persona['paterno']
    materno=persona['materno']
    edad=persona['edad']
    sql = "insert into persona (nombre,paterno,materno,edad) values (%s,%s,%s,%s)"
    cursor.execute(sql,(nombre,paterno,materno,edad))
    conection.commit()
    return jsonify(persona)


@app.route('/api/update', methods = ['POST'])
def update():
    persona = request.get_json()
    id= persona['idpersona']
    nombre = persona['nombre']
    paterno = persona['paterno']
    materno = persona['materno']
    edad = persona['edad']
    sql = "UPDATE persona SET nombre = %s, paterno = %s,materno = %s, edad = %s WHERE idpersona=%s"
    cursor.execute(sql, (nombre, paterno, materno, edad,id))
    conection.commit()
    return jsonify(persona)

@app.route('/api/delete', methods = ['POST'])
def delete():
    persona = request.get_json()
    id= persona['idpersona']
    print(persona)
    sql = "DELETE FROM persona WHERE idpersona = %s"
    cursor.execute(sql, (id))
    conection.commit()
    return("borrado")

if __name__ == '__main__':
    app.run()
